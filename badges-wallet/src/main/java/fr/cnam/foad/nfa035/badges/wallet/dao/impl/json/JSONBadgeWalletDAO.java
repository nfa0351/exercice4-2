package fr.cnam.foad.nfa035.badges.wallet.dao.impl.json;

import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;

import java.io.IOException;
import java.io.OutputStream;

public interface JSONBadgeWalletDAO extends DirectAccessBadgeWalletDAO {

    public void getBadgeFromMetadata(OutputStream imageStream, DigitalBadge meta) throws IOException;

}
